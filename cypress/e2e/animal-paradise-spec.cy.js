/// <reference types="Cypress" />

describe("animal paradise spec", () => {
  beforeEach(() => {
    cy.visit("/");
    cy.wait(1000);
  });

  it("user visit the webpage", () => {
    cy.get("#category_cats").click();
    cy.get("#curiositiesPage").click();
    cy.scrollTo("bottom");
    cy.scrollTo("top");
    cy.get("#backButton").click();
    cy.get("#category_dogs").should("exist");
  });

  it("user create a new animal", () => {
    cy.get("#inputName").type("robert");
    cy.get("#inputBreed").type("bodeguero");
    cy.get("#inputImg").type(
      "https://trofeocaza.com/wp-content/uploads/2016/06/perros-razas-ratonero-bodeguero-andaluz-01.jpg"
    );
    cy.get("#addButton > .button").click();
    cy.get("#name_robert").should("exist");
    cy.get("#breed_bodeguero").should("exist");
    cy.get(
      'img[src="https://trofeocaza.com/wp-content/uploads/2016/06/perros-razas-ratonero-bodeguero-andaluz-01.jpg"]'
    ).should("exist");
  });

  it("user create an animal but cancel", () => {
    cy.get("#inputName").type("robert");
    cy.get("#inputBreed").type("bodeguero");
    cy.get("#inputImg").type(
      "https://trofeocaza.com/wp-content/uploads/2016/06/perros-razas-ratonero-bodeguero-andaluz-01.jpg"
    );
    cy.get("#cancelButton > .button").click();
  });
  it("user create a new animal in other category", () => {
    cy.get("#category_cats").click();

    cy.get("#inputName").type("minirro");
    cy.get("#inputBreed").type("siamese");
    cy.get("#inputImg").type(
      "https://upload.wikimedia.org/wikipedia/commons/6/60/Neighbours_Siamese.jpg"
    );
    cy.get("#addButton > .button").click();
    cy.get("#breed_siamese").should("exist");
    cy.get("#category_dogs").click();
    cy.get("#breed_siamese").should("not.exist");
  });

  it("user add a new animal category", () => {
    cy.get("#inputSpeciesName").type("birds");
    cy.get("#addSpeciesButton > .button").click();
    cy.get("#category_birds").should("exist");
  });
});

// it("user update an animal", () => {
//   cy.get('#animal_645a66526246ac03e853b3f5').click();
//   cy.get("#inputName").type("robertsete");
//   cy.get("#inputBreed").type("perruso");
//   cy.get("#inputImg").type(
//     "https://i.blogs.es/c8143a/ratonero-bodeguero/840_560.jpeg"
//   );
//   cy.get("#updateButton > .button").click();
//   cy.get('#animal_645a66526246ac03e853b3f5 > ui-animal-detail > #name_roberto').should("not.exist");
//   cy.get('#animal_645a66526246ac03e853b3f5 > ui-animal-detail > #name_robertsete').should("exist");
//   cy.get("#updateButton > .button").should("not.be.visible");
//   cy.get("#deleteButton > .button").should("not.be.visible");
//   cy.get("#cancelButton > .button").should("be.visible");
//   cy.get("#addButton > .button").should("be.visible");
// });

// it("user delete an animal", () => {
//   cy.get('#animal_645a66526246ac03e853b3f5').click();
//   cy.get("#deleteButton > .button").click();
//   cy.get('#animal_645a66526246ac03e853b3f5').should("not.exist");
//   cy.get("#updateButton > .button").should("not.be.visible");
//   cy.get("#deleteButton > .button").should("not.be.visible");
//   cy.get("#cancelButton > .button").should("be.visible");
//   cy.get("#addButton > .button").should("be.visible");
// });
