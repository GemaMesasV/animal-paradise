import { Animal } from "../src/model/animal";
import { AnimalsRepository } from "../src/repositories/animals.repository";
import { CreateAnimalUseCase } from "../src/usecases/create-animal.usecase";

import { ANIMALS } from "./fixtures/animals";

jest.mock("../src/repositories/animals.repository");

describe("Create animal use case", () => {
  beforeEach(() => {
    AnimalsRepository.mockClear();
  });

  it("should executing properly", async () => {
    const animal = new Animal({
      name: "Animal created",
      breed: "Breed created",
      img: "https://soyunperro.com/wp-content/uploads/2019/11/Chihuahua-en-el-jard%C3%ADn-770x470.jpg",
      _id: "611a64a2e0a8f50009a06123",
    });

    AnimalsRepository.mockImplementation(() => {
      return {
        createAnimal: () => {
          return {
            name: animal.name,
            breed: animal.breed,
            img: animal.img,
            _id: animal._id,
          };
        },
      };
    });

    const animalsCreated = await CreateAnimalUseCase.execute(ANIMALS, animal);

    expect(animalsCreated.length).toBe(4);
    expect(animalsCreated[0].name).toBe(animal.name);
    expect(animalsCreated[0].breed).toBe(animal.breed);
    expect(animalsCreated[0].img).toBe(animal.img);
  });
});
