import { Animal } from "../src/model/animal";
import { AnimalsRepository } from "../src/repositories/animals.repository";
import { DeleteAnimalUseCase } from "../src/usecases/delete-animal.usecase";

import { ANIMALS } from "./fixtures/animals";

jest.mock("../src/repositories/animals.repository");

describe("Delete animal use case", () => {
  beforeEach(() => {
    AnimalsRepository.mockClear();
  });

  it("should executing properly", async () => {
    AnimalsRepository.mockImplementation(() => {
      return {
        deleteAnimal: () => {
          return 200;
        },
      };
    });

    const animalsDeleted = await DeleteAnimalUseCase.execute(
      ANIMALS,
      "611a64a2e0a8f50009a06d49"
    );

    expect(animalsDeleted.length).toBe(2);
    expect(animalsDeleted[1].name).toBe(ANIMALS[2].name);
    expect(animalsDeleted[1].breed).toBe(ANIMALS[2].breed);
    expect(animalsDeleted[1].img).toBe(ANIMALS[2].img);
  });
});
