import { AnimalsRepository } from "../src/repositories/animals.repository";
import { AllAnimalsUseCase } from "../src/usecases/all-animals.usecase";

import { ANIMALS } from "./fixtures/animals";

jest.mock("../src/repositories/animals.repository");

describe("All animals use case", () => {
  beforeEach(() => {
    AnimalsRepository.mockClear();
  });

  it("should get all animals", async () => {
    AnimalsRepository.mockImplementation(() => {
      return {
        getAllAnimals: () => ANIMALS,
      };
    });

    const result = await AllAnimalsUseCase.execute();

    expect(result.length).toBe(ANIMALS.length);
    expect(result[0].name).toBe(ANIMALS[0].name);
    expect(result[0].breed).toBe(ANIMALS[0].breed);
    expect(result[0].img).toBe(ANIMALS[0].img);
    expect(result[0]._id).toBe(ANIMALS[0]._id);
  });
});
