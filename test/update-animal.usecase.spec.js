import { Animal } from "../src/model/animal";
import { AnimalsRepository } from "../src/repositories/animals.repository";
import { UpdateAnimalUseCase } from "../src/usecases/update-animal.usecase";

import { ANIMALS } from "./fixtures/animals";

jest.mock("../src/repositories/animals.repository");

describe("Update animal use case", () => {
  beforeEach(() => {
    AnimalsRepository.mockClear();
  });

  it("should executing properly", async () => {
    const animal = new Animal({
      name: "name updated",
      breed: "breed updated",
      img: "https://fello.pet/wp-content/uploads/2020/12/zolotistyj-retriver2-scaled.jpg",
      _id: "611a64a2e0a8f50009a06d49",
    });

    AnimalsRepository.mockImplementation(() => {
      return {
        updateAnimal: () => {
          return 200;
        },
      };
    });

    const animalsUpdated = await UpdateAnimalUseCase.execute(ANIMALS, animal);

    expect(animalsUpdated.length).toBe(3);
    expect(animalsUpdated[1].name).toBe(animal.name);
    expect(animalsUpdated[1].breed).toBe(animal.breed);
    expect(animalsUpdated[1].img).toBe(animal.img);
    expect(animalsUpdated[1]._id).toBe(animal._id);
  });
});
