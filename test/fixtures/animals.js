export const ANIMALS = [
  {
    name: "Luna",
    breed: "Husky",
    img: "https://res.cloudinary.com/dktx1oojk/image/upload/f_auto,dpr_auto,q_60,fl_progressive/w_0.8888392857142857142857142857,h_1.0,c_fill,g_auto/w_1536,h_1152,c_scale/www2022/powerinit/15458/Print_AniCura_Dogs_352_Siberian%20husky.jpg",
    _id: "611a647ee0a8f50009a06d47",
  },
  {
    name: "Simba",
    breed: "Golden Retriever",
    img: "https://soyunperro.com/wp-content/uploads/2018/01/perro-golden-retriever-770x470.jpg",
    _id: "611a64a2e0a8f50009a06d49",
  },
  {
    name: "Garfield",
    breed: "Persian",
    img: "https://nfnatcane.es/blog/wp-content/uploads/2022/03/gato-persa.jpg",
    _id: "611a64c2e0a8f50009a06d4b",
  },
];
