# Animal Paradise by GemaMesasV

![Logo](src/images/animal-paradise-logo.png)

This application has been developed based on WebComponents application architecture with LitElement, Routing with Vaadin and global state management of the application with Valtio.<br><br>
The application connects to an external API called "crudcrud". This API allows to make GET, POST, PUT and DELETE requests on any type of data; in this case animals.<br><br>
Each use case has its associated unit test using Jest. Linter is also used for commit messages.<br><br>
The changes go through a GitLab pipeline that runs the tests, generates an image in Docker Hub and deploys the application to Firebase.<br>
Also, the project has a multistage docker-compose file to be able to build the application locally.<br><br>
Finally, Conventional Commits has been used when developing the project. Likewise, good practices and accessibility measures have been followed as far as possible in the development of this application.<br>
For example, the following web page was used when checking the colors: https://www.siegemedia.com/contrast-ratio#%23584335-on-%233ad97a

## IMPORTANT

Due to the limitations of the API, it is only able to make 100 requests and be available for 24h, so to use the application once you have cloned the repo you will have to update the .env file with your new key from: https://crudcrud.com/
<br>
<br>
Testing with cypress requires many API requests so be sure to generate a new API Key before testing.<br>
Similarly, the update and delete animals tests are disabled because the id that is generated with the creation of an animal is lost when using a new API key.

## Installation

Clone the repo:

`git clone git@gitlab.com:GemaMesasV/animal-paradise.git `

Install NPM packages

`npm install`

## Usage

To run the app you need to use the following command:

`npm run start`

## Functionalities that I would like to implement in the future:

- Responsive design
- Improve /curiosities so the user can add curiosities to the list.
- Validation of inputs
