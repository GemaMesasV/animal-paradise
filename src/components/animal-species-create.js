import { LitElement, html } from "lit";
import "../ui/ui-animal-detail";
import "../ui/ui-button";

import { state } from "../states/state";

export class AnimalSpeciesCreate extends LitElement {
  connectedCallback() {
    super.connectedCallback();
    this.nameValue = "";
  }

  static get properties() {
    return {
      nameValue: { type: String },
    };
  }

  render() {
    return html`
      <section>
        <h2 class="title">Add species</h2>
        <form class="form form-row" action="">
          <label class="form__label"
            >New species<input
              id="inputSpeciesName"
              class="form__label--input"
              type="text"
              @change="${this.setNameInput}"
              .value="${this.nameValue}"
              placeholder="e.g. Rabbits"
              size="50"
          /></label>
          <div>
            <ui-button
              id="addSpeciesButton"
              type="reset"
              @click="${this.addClick}"
              buttonContent="Add"
            >
            </ui-button>
          </div>
        </form>
        <div class="curiosities-info">
          <a id="curiositiesPage" href="/curiosities"
            >Do you want to know some curiosities about animals? 👆
          </a>
        </div>
      </section>
    `;
  }

  setNameInput(e) {
    this.nameValue = e.target.value;
  }

  resetForm() {
    this.nameValue = "";
  }

  createRenderRoot() {
    return this;
  }

  async addClick(e) {
    e.preventDefault();
    if (this.nameValue) {
      state.categories = [...state.categories, this.nameValue];
      state.selectedCategory = this.nameValue;
      this.resetForm();
    }
  }
}

customElements.define("animal-species-create", AnimalSpeciesCreate);
