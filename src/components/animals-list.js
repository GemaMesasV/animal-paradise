import { LitElement, html } from "lit";
import { AllAnimalsUseCase } from "../usecases/all-animals.usecase";
import { state } from "../states/state";
import { snapshot, subscribe } from "valtio/vanilla";
import "../ui/ui-animal-detail";

export class AnimalsList extends LitElement {
  static get properties() {
    return {
      animals: { type: Array },
      selectedCategory: { type: String },
    };
  }

  async connectedCallback() {
    super.connectedCallback();

    this.unsubscribe = subscribe(state, async () => {
      this.animals = snapshot(state).animals;
      if (state.selectedCategory !== this.selectedCategory) {
        this.selectedCategory = state.selectedCategory;
        state.animals = await AllAnimalsUseCase.execute(state.selectedCategory);
      }
    });
    state.animals = await AllAnimalsUseCase.execute(state.selectedCategory);
  }
  disconnectedCallback() {
    this.unsubscribe();
  }
  render() {
    return html`
      <button @click="${this.addModeClick}" class="button">Add Animal</button>
      <h1 class="title">Animals list</h1>
      <ul class="animal__list">
        ${this.animals?.map(
          (animal) =>
            html`<li
              id="animal_${animal._id}"
              @click="${() => this.animalSelectedClick(animal)}"
              class="animal__list--item"
            >
              <ui-animal-detail .animal=${animal}></ui-animal-detail>
            </li>`
        )}
      </ul>
    `;
  }

  createRenderRoot() {
    return this;
  }
  animalSelectedClick(animal) {
    state.selectedAnimal = animal;
  }

  addModeClick(e) {
    e.preventDefault();
    state.selectedAnimal = {
      name: "",
      breed: "",
      img: "",
      _id: "",
    };
  }
}

customElements.define("animals-list", AnimalsList);
