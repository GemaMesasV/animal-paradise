import { LitElement, html } from "lit";
export class CuriositiesList extends LitElement {
  render() {
    return html`
      <h1 class="title">Curiosities</h1>
      <h1 class="subtitle">Did you know that...?</h1>
      <button id="backButton" onClick="location.href='/'" class="button">
        Back to home
      </button>
      <ul class="list">
        <li class="list__li">
          Elephants: Elephants are the only mammals that can't jump.
        </li>
        <li class="list__li">
          Giraffes: Giraffes have the same number of neck vertebrae as humans:
          7.
        </li>
        <li class="list__li">
          Penguins: Penguins have a gland that filters salt from their
          bloodstream, allowing them to drink seawater.
        </li>
        <li class="list__li">Kangaroos: Kangaroos can't walk backwards.</li>
        <li class="list__li">
          Owls: Owls are the only birds that can see the color blue.
        </li>
        <li class="list__li">
          Crocodiles: Crocodiles can't stick their tongues out.
        </li>
        <li class="list__li">
          Platypuses: Platypuses are one of the few mammals that lay eggs
          instead of giving birth.
        </li>
        <li class="list__li">Bees: Bees can recognize human faces.</li>
        <li class="list__li">
          Chameleons: Chameleons can move their eyes independently, allowing
          them to look in two different directions at the same time.
        </li>
        <li class="list__li">
          Sharks: Sharks can sense electric fields, which helps them locate
          prey.
        </li>
        <li class="list__li">
          Cats: Cats have five toes on their front paws, but only four toes on
          their back paws.
        </li>
        <li class="list__li">Dolphins: Dolphins sleep with one eye open.</li>
        <li class="list__li">
          Rabbits: Rabbits have two sets of front teeth, one behind the other.
        </li>
        <li class="list__li">Octopuses: Octopuses have three hearts.</li>
        <li class="list__li">Horses: Horses can't vomit.</li>
        <li class="list__li">
          Ants: Ants can carry objects that weigh up to 50 times their body
          weight.
        </li>
        <li class="list__li">
          Flamingos: Flamingos are born gray, but their feathers turn pink due
          to the food they eat.
        </li>
        <li class="list__li">
          Chimpanzees: Chimpanzees use tools to obtain food, such as using
          sticks to extract termites from their nests.
        </li>
        <li class="list__li">
          Camels: Camels can drink up to 30 gallons of water in just 13 minutes.
        </li>
        <li class="list__li">Bats: Bats are the only mammals that can fly.</li>
        <li class="list__li">
          Snails: Snails can sleep for up to three years.
        </li>
        <li class="list__li">
          Geckos: Geckos can stick to almost any surface due to the microscopic
          hairs on their toes.
        </li>
        <li class="list__li">
          Hyenas: Hyenas' bite force is strong enough to crush bones.
        </li>
        <li class="list__li">Starfish: Starfish can regenerate lost limbs.</li>
        <li class="list__li">
          Butterflies: Butterflies taste with their feet.
        </li>
        <li class="list__li">
          Peacocks: Peacocks use their colorful feathers to attract mates.
        </li>
        <li class="list__li">
          Koalas: Koalas have fingerprints that are similar to human
          fingerprints.
        </li>
        <li class="list__li">
          Scorpions: Scorpions glow under ultraviolet light.
        </li>
        <li class="list__li">
          Beavers: Beavers have transparent eyelids that allow them to see
          underwater.
        </li>
        <li class="list__li">
          Seahorses: Male seahorses carry and give birth to their young.
        </li>
      </ul>
    `;
  }
  createRenderRoot() {
    return this;
  }
}

customElements.define("curiosities-list", CuriositiesList);
