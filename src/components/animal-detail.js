import { LitElement, html } from "lit";
import "../ui/ui-animal-detail";
import "../ui/ui-button";
import { CreateAnimalUseCase } from "../usecases/create-animal.usecase";
import { DeleteAnimalUseCase } from "../usecases/delete-animal.usecase";
import { UpdateAnimalUseCase } from "../usecases/update-animal.usecase";
import { subscribe } from "valtio/vanilla";

import { state } from "../states/state";
import { Animal } from "../model/animal";

export class AnimalDetail extends LitElement {
  static get properties() {
    return {
      nameValue: { type: String },
      breedValue: { type: String },
      imgValue: { type: String },
      animalId: { type: String },
    };
  }

  connectedCallback() {
    super.connectedCallback();

    this.unsubscribe = subscribe(state, async () => {
      this.nameValue = state.selectedAnimal.name;
      this.breedValue = state.selectedAnimal.breed;
      this.imgValue = state.selectedAnimal.img;
      this.animalId = state.selectedAnimal._id;
      this.render();
    });
  }

  static get properties() {
    return {
      nameValue: { type: String },
      breedValue: { type: String },
      imgValue: { type: String },
      animalId: { type: Number },
    };
  }

  render() {
    return html`
      <section>
        <h2 class="title">Animal detail</h2>
        <form class="form" action="">
          <label class="form__label"
            >Name<input
              id="inputName"
              class="form__label--input"
              type="text"
              @change="${this.setNameInput}"
              .value="${this.nameValue}"
              placeholder="e.g. Wabbit"
              size="50"
          /></label>
          <label class="form__label"
            >Breed<input
              id="inputBreed"
              class="form__label--input"
              type="text"
              @change="${this.setBreedInput}"
              .value="${this.breedValue}"
              placeholder="e.g. Angora"
              size="50"
          /></label>
          <label class="form__label"
            >Image<input
              id="inputImg"
              class="form__label--input"
              type="text"
              @change="${this.setImgInput}"
              .value="${this.imgValue}"
              placeholder="https://example.com/example.jpg"
              size="50"
          /></label>
          <div>
            <ui-button
              id="addButton"
              type="reset"
              @click="${this.addClick}"
              ?hidden="${this.animalId}"
              buttonContent="Add"
            >
            </ui-button>
            <ui-button
              id="cancelButton"
              type="reset"
              @click="${this.cancelClick}"
              buttonContent="Cancel"
            ></ui-button>
            <ui-button
              id="updateButton"
              type="submit"
              @click="${this.updateClick}"
              ?hidden="${!this.animalId}"
              buttonContent="Update"
            >
            </ui-button>
            <ui-button
              id="deleteButton"
              type="submit"
              @click="${this.deleteClick}"
              ?hidden="${!this.animalId}"
              buttonContent="Delete"
            >
            </ui-button>
          </div>
        </form>
      </section>
    `;
  }

  setNameInput(e) {
    this.nameValue = e.target.value;
  }
  setBreedInput(e) {
    this.breedValue = e.target.value;
  }
  setImgInput(e) {
    this.imgValue = e.target.value;
  }

  resetForm() {
    state.selectedAnimal = {
      name: "",
      breed: "",
      img: "",
      _id: "",
    };
  }

  cancelClick(e) {
    e.preventDefault();
    this.resetForm();
  }

  async addClick(e) {
    e.preventDefault();
    const newAnimal = new Animal({
      name: this.nameValue,
      breed: this.breedValue,
      img: this.imgValue,
    });
    const updatedAnimals = await CreateAnimalUseCase.execute(
      state.animals,
      newAnimal,
      state.selectedCategory
    );
    state.animals = updatedAnimals;
    this.resetForm();
  }

  async deleteClick(e) {
    e.preventDefault();
    const updatedAnimals = await DeleteAnimalUseCase.execute(
      state.animals,
      this.animalId,
      state.selectedCategory
    );
    state.animals = updatedAnimals;
    this.resetForm();
  }

  async updateClick(e) {
    e.preventDefault();
    const updatedAnimal = new Animal({
      _id: this.animalId,
      name: this.nameValue,
      breed: this.breedValue,
      img: this.imgValue,
    });
    const updatedAnimals = await UpdateAnimalUseCase.execute(
      state.animals,
      updatedAnimal,
      state.selectedCategory
    );
    state.animals = updatedAnimals;
    this.resetForm();
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("animal-detail", AnimalDetail);
