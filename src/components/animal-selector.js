import { LitElement, html } from "lit";
import { state } from "../states/state";
import { snapshot, subscribe } from "valtio/vanilla";

export class AnimalsSelector extends LitElement {
  static get properties() {
    return {
      categories: { type: Array },
      selectedCategory: { type: String },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.categories = snapshot(state).categories;
    this.selectedCategory = snapshot(state).selectedCategory;

    this.unsubscribe = subscribe(state, () => {
      this.categories = snapshot(state).categories;
      this.selectedCategory = snapshot(state).selectedCategory;
      this.render();
    });
  }
  disconnectedCallback() {
    this.unsubscribe();
  }
  render() {
    return html`
      <nav>
        <ul class="nav__list">
          ${this.categories?.map(
            (category) =>
              html`<li
                id="category_${category}"
                @click="${() => (state.selectedCategory = category)}"
                class="nav__list--item ${this.selectedCategory === category
                  ? "nav__list--item--selected"
                  : ""}"
              >
                ${category}
              </li>`
          )}
        </ul>
      </nav>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("animals-selector", AnimalsSelector);
