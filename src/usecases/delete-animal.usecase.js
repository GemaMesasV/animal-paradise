import { AnimalsRepository } from "../repositories/animals.repository";

export class DeleteAnimalUseCase {
  static async execute(animals = [], _id, category) {
    const repository = new AnimalsRepository();
    const deleteStatus = await repository.deleteAnimal(category, _id);
    return deleteStatus === 200
      ? animals.filter((animal) => animal._id != _id)
      : animals;
  }
}
