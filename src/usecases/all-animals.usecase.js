import { AnimalsRepository } from "../repositories/animals.repository";

export class AllAnimalsUseCase {
  static async execute(category) {
    const repository = new AnimalsRepository();
    const animals = await repository.getAllAnimals(category);
    return animals.map((animal) => ({
      _id: animal._id,
      name: animal.name,
      breed: animal.breed,
      img: animal.img,
    }));
  }
}
