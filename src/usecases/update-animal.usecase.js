import { AnimalsRepository } from "../repositories/animals.repository";

export class UpdateAnimalUseCase {
  static async execute(animals = [], animal, category) {
    const repository = new AnimalsRepository();
    const updateStatus = await repository.updateAnimal(category, animal);
    return updateStatus === 200
      ? animals.map((anml) => (anml._id === animal._id ? animal : anml))
      : animals;
  }
}
