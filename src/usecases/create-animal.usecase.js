import { AnimalsRepository } from "../repositories/animals.repository";

export class CreateAnimalUseCase {
  static async execute(animals = [], animal, category) {
    const repository = new AnimalsRepository();
    const animalCreated = await repository.createAnimal(category, animal);
    const animalModelCreated = {
      _id: animalCreated._id,
      name: animalCreated.name,
      breed: animalCreated.breed,
      img: animalCreated.img,
    };

    return [animalModelCreated, ...animals];
  }
}
