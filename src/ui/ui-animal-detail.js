import { LitElement, html } from "lit";
import "../components/animals-list";

export class UiAnimalDetail extends LitElement {
  static get properties() {
    return {
      animal: { type: Object },
    };
  }

  render() {
    return html`<p id="name_${this.animal?.name}" class="ui-animal-title">
        ${this.animal?.name}
      </p>
      <p id="breed_${this.animal?.breed}" class="ui-animal-subtitle">
        ${this.animal?.breed}
      </p>
      <img
        id="img-animal_${this.animal?.img}"
        class="ui-animal-img"
        src=${this.animal?.img}
        alt="animal"
      /> `;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("ui-animal-detail", UiAnimalDetail);
