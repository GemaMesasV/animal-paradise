import axios from "axios";

export class AnimalsRepository {
  async getAllAnimals(category) {
    return await (
      await axios.get(
        `https://crudcrud.com/api/${process.env.CRUDCRUD_API_KEY}/${category}`
      )
    ).data;
  }

  async createAnimal(category, animal) {
    return await (
      await axios.post(
        `https://crudcrud.com/api/${process.env.CRUDCRUD_API_KEY}/${category}`,
        animal
      )
    ).data;
  }

  async deleteAnimal(category, _id) {
    return await (
      await axios.delete(
        `https://crudcrud.com/api/${process.env.CRUDCRUD_API_KEY}/${category}/${_id}`
      )
    ).status;
  }

  async updateAnimal(category, animal) {
    const animalDto = {
      name: animal.name,
      breed: animal.breed,
      img: animal.img,
    };
    return await (
      await axios.put(
        `https://crudcrud.com/api/${process.env.CRUDCRUD_API_KEY}/${category}/${animal._id}`,
        animalDto
      )
    ).status;
  }
}
