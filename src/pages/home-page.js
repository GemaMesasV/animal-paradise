import "../components/animals-list";
import "../components/animal-detail";
import "../components/animal-selector";
import "../components/animal-species-create";

export class HomePage extends HTMLElement {
  constructor() {
    super();
  }

  getStyles() {
    return `
    <style>
      home-page {
        width: 100%;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        gap: 16px;
      }
    </style>
    `;
  }

  async connectedCallback() {
    this.innerHTML = `${this.getStyles()}
    <animals-selector class="animals-selector-section" id="animalSelector"></animals-selector>
    <div class="forms">
    <animal-species-create class="animal-species-create-section" id="animalSpeciesCreate"></animal-species-create>
    <animal-detail class="animal-detail-section" id="animalDetail"></animal-detail></div>
    <animals-list class="animals-list-section" id="animalsList"></animals-list>
    `;
  }
}
customElements.define("home-page", HomePage);
