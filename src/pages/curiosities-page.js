import "../components/curiosities-list";
export class curiositiesPage extends HTMLElement {
  constructor() {
    super();
  }

  getStyles() {
    return `
    <style>
    home-page {
      width: 100%;
      height: 100%;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      gap: 16px;
    }
    </style>
    `;
  }

  connectedCallback() {
    this.innerHTML = `${this.getStyles()}
    <curiosities-list class="curiosities-page-section"></curiosities-list>
    `;
  }
}
customElements.define("curiosities-page", curiositiesPage);
