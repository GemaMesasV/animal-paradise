import { proxy, subscribe } from "valtio/vanilla";
export const state = proxy(
  JSON.parse(localStorage.getItem("state")) || {
    animals: [],
    categories: ["dogs", "cats"],
    selectedCategory: "dogs",
    selectedAnimal: {
      name: "",
      breed: "",
      img: "",
      _id: "",
    },
  }
);

subscribe(state, () => {
  localStorage.setItem("state", JSON.stringify(state));
});
