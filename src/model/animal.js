export class Animal {
  constructor({ _id, name, breed, img }) {
    this._id = _id;
    this.name = name;
    this.breed = breed;
    this.img = img;
  }
}
