import "./styles/main.css";
import "./styles/components/header.css";
import "./styles/components/main.css";
import "./styles/components/animals-list-section.css";
import "./styles/components/animal-detail-section.css";
import "./styles/components/animal-selector-section.css";
import "./styles/components/forms.css";
import "./styles/components/animal-species-create-section.css";
import "./styles/components/curiosities-page-section.css";

import "./styles/core/reset.css";
import "./styles/core/variables.css";
import { Router } from "@vaadin/router";

import "./pages/home-page";
import "./pages/curiosities-page";

const outlet = document.querySelector("#outlet");
const router = new Router(outlet);

router.setRoutes([
  { path: "/", component: "home-page" },
  { path: "/curiosities", component: "curiosities-page" },
  { path: "(.*)", redirect: "/" },
]);
